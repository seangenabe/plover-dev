#!/usr/bin/env -S deno run --allow-read --allow-write

const Ctrl = (s: string) => `Control_L(${s})`
const Meta = (s: string) => `Super_L(${s})`

const modifiers: { stroke: string; modifiers(stroke: string): string }[] = [
  {
    stroke: "STPH",
    modifiers: (s) => s,
  },
  {
    stroke: "STPHR",
    modifiers: (s) => `Shift_L(${s})`,
  },
]

const keys = [
  {
    stroke: "-P",
    keys: "Up",
  },
  {
    stroke: "-B",
    keys: "Down",
  },
  {
    stroke: "-R",
    keys: "Left",
  },
  {
    stroke: "-G",
    keys: "Right",
  },
  {
    stroke: "-RB",
    keys: "Control_L(Left)",
  },
  {
    stroke: "-BG",
    keys: "Control_L(Right)",
  },
  {
    stroke: "-FPL",
    keys: "Home",
  },
  {
    stroke: "-RBG",
    keys: "End",
  },
  {
    stroke: "-F",
    keys: "Backspace",
  },
  {
    stroke: "-L",
    keys: "Delete",
  },
  {
    stroke: "-FP",
    keys: "Control_L(Backspace)",
  },
  {
    stroke: "-PL",
    keys: "Control_L(Delete)",
  },
  {
    stroke: "ER",
    keys: "Alt_L(Left)",
  },
  {
    stroke: "EG",
    keys: "Alt_L(Right)",
  },
  {
    stroke: "EP",
    keys: "Alt_L(Up)",
  },
  {
    stroke: "EB",
    keys: "Alt_L(Down)",
  },
  {
    stroke: "-FRPB",
    keys: "Control_L(Home)",
  },
  {
    stroke: "-PBLG",
    keys: "Control_L(End)",
  },
  {
    stroke: "UPL",
    keys: "PageUp",
  },
  {
    stroke: "UBG",
    keys: "PageDown",
  },
]

const windowNav: { stroke: string; keys: string }[] = [
  {
    stroke: "-F",
    keys: Meta("U"),
  },
  {
    stroke: "-P",
    keys: Meta("I"),
  },
  {
    stroke: "-L",
    keys: Meta("O"),
  },
  {
    stroke: "-FR",
    keys: Meta("J"),
  },
  {
    stroke: "-PB",
    keys: Meta("K"),
  },
  {
    stroke: "-LG",
    keys: Meta("L"),
  },
  {
    stroke: "-R",
    keys: Meta("M"),
  },
  {
    stroke: "-B",
    keys: Meta("Comma"),
  },
  {
    stroke: "-G",
    keys: Meta("Period"),
  },
  {
    stroke: "-FP",
    keys: Meta(Ctrl("U")),
  },
  {
    stroke: "-FPL",
    keys: Meta(Ctrl("I")),
  },
  {
    stroke: "-PL",
    keys: Meta(Ctrl("O")),
  },
  {
    stroke: "-FRPB",
    keys: Meta(Ctrl("J")),
  },
  {
    stroke: "-FRPBLG",
    keys: Meta(Ctrl("K")),
  },
  {
    stroke: "-PBLG",
    keys: Meta(Ctrl("L")),
  },
  {
    stroke: "-RB",
    keys: Meta(Ctrl("M")),
  },
  {
    stroke: "-RBG",
    keys: Meta(Ctrl("Comma")),
  },
  {
    stroke: "-BG",
    keys: Meta(Ctrl("Period")),
  },
  {
    stroke: "-FRP",
    keys: Meta("Home"),
  },
  {
    stroke: "-PLG",
    keys: Meta("End"),
  },
  {
    stroke: "-FRB",
    keys: Meta("Page_Up"),
  },
  {
    stroke: "-BLG",
    keys: Meta("Page_Down"),
  },
  {
    stroke: "-FRG",
    keys: Meta("Left"),
  },
  {
    stroke: "-RLG",
    keys: Meta("Right"),
  },
  {
    stroke: "-RPBG",
    keys: Meta("Down"),
  },
  {
    stroke: "-FRLG",
    keys: Meta("Up"),
  },
  {
    stroke: "-RPG",
    keys: Meta(Ctrl("Down")),
  },
  {
    stroke: "-FBL",
    keys: Meta(Ctrl("Page_Up")),
  },
]

const output: Record<string, string> = Object.fromEntries([
  ...modifiers.flatMap((mod) =>
    keys.map((key) => [
      `${mod.stroke}${key.stroke}`,
      `{#${mod.modifiers(key.keys)}}`,
    ])
  ),
  ...windowNav
    .map((key) => ({ ...key, stroke: `KPR${key.stroke}` }))
    .map((key) => [key.stroke, `{#${key.keys}}`]),
])

await Deno.writeTextFile("./navigation.json", JSON.stringify(output, null, 2))
